// When the user scrolls the page, execute executeSticky
window.onscroll = function() { executeSticky() };

// Get the header
const header = document.getElementById("stickyHeader");

// Get the offset position of the navbar
const sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
const executeSticky = () => {
  (window.pageYOffset > sticky) ? header.classList.add("sticky") : header.classList.remove("sticky");
}