// General structure of the page.
// User only can see div#products or div#cart.
// User only can see button#btn-showCart or button#btn-back.
// With button#btn-showCart user can access to div#cart from div#products.
// With button#btn-back user can access to div#products from div#cart.
document.getElementById('main').innerHTML = `
    <header id="stickyHeader">
    <h1>FilmShop</h1>
    <div class="empty"></div>
    <button id="btn-showCart">Ver carrito</button>
    <button id="btn-back">Volver</button>
    </header>
    <div id="products"></div>
    <div id="cart">
        <table id="tableCart">
        </table>
        <button class="cartBtn">Comprar</button>
    </div>
`;

// A film is a product stored inside div#products
// It takes data from web API.
const film = (imgUrl, title, dataInfo, id) => {
    return `
    <article>
    <div class="film" id="film_${id}">
        <img src="${imgUrl}">
        <h2 class="title">${title}</h2>
        <p class="info">${dataInfo}</p>
        <button id="${id}">Añadir al carrito</button>
    </div>
    </article>
`}

// A card is an element from shoping cart.
// It takes data from dataCart object.
const cardCart = (imgUrl, title, dataInfo, id) => {
    return `
    <tr class="tableCard" id="card_${id}">
        <td><img src="${imgUrl}"></td>
        <td><h2 class="cardTitle"><strong>${title}:</strong></h2><p class="cardInfo">${dataInfo}</p></td>
        <td><button class="trash" id="trash_${id}"><i class="fas fa-trash-alt fa-2x"></i></button></td>
    </tr>
`}

// When you click "añadir al carrito"
// the film is stored in the dataCart
// and then, when the user visit the cart, he can see stored films and can remove them from the cart.
const addToCartBtn = (id, film) => {
    dataCart[id] = film;
    addProductToCart(dataCart, id);
    addListenersCartBtn(dataCart, id);
    document.getElementById('film_'+id).style.display = 'none';
}

// This function show the films inside div#products
const addProducts = dataArr => {
    const dataArrLength = dataArr.length;
    for (let i = 0; i < dataArrLength; i++) {
        const id = 'id_' + dataArr[i].imdbID;
        document.getElementById('products').insertAdjacentHTML('beforeend', film(dataArr[i].Poster, dataArr[i].Title, dataArr[i].Plot, id));
    }
}

// This function let to add a film to the cart when the user clicks "añadir al carrito"
const addListenersProdBtn = dataArr => {
    const dataArrLength = dataArr.length;
    for (let i = 0; i < dataArrLength; i++) {
        const id = 'id_' + dataArr[i].imdbID;
        document.getElementById(id).addEventListener('click', () => {
            addToCartBtn(id, dataArr[i]);
        })
    }
}

// This function let to remove a film from the cart when the user clicks the trash button.
const addListenersCartBtn = (dataCart, id) => {
    console.log('addListenersCartBtn ', id);
    document.getElementById('trash_'+id).addEventListener('click', () => {
        deleteProductFromCart(dataCart, id);
    });
}

// This function show the films inside div#cart in the correspondent table
const addProductToCart = (dataCart, id) => {
    document.getElementById('tableCart').insertAdjacentHTML('beforeend', cardCart(dataCart[id].Poster, dataCart[id].Title, dataCart[id].Plot, id));
}

// When the user click the trash button, 
// the correspondent film is deleted from the cart
// and returns to div#products
const deleteProductFromCart = (dataCart, id) => {
    document.getElementById('card_'+id).remove();
    delete dataCart[id];
    document.getElementById('film_'+id).style.display = 'flex';
}

// This function let to change between #btn-showCart ("ver carrito") and #btn-back ("volver")
const btnHeaderFunction = () => {
    document.getElementById('btn-showCart').addEventListener('click', () => {
        showCartView();
    });
    document.getElementById('btn-back').addEventListener('click', () => {
        showProductsView();
    });
}

// When the user clicks "volver" in the cart view,
// cart view is hidden and products view is shown.
const showProductsView = () => {
    document.getElementById('products').style.display = 'flex';
    document.getElementById('btn-showCart').style.display = 'block';
    document.getElementById('cart').style.display = 'none';
    document.getElementById('btn-back').style.display = 'none';
}

// When the user clicks "ver carrito" in the products view,
// products view is hidden and cart view is shown.
const showCartView = () => {
    document.getElementById('cart').style.display = 'flex';
    document.getElementById('btn-back').style.display = 'block';
    document.getElementById('products').style.display = 'none';
    document.getElementById('btn-showCart').style.display = 'none';
}

// We need to wait data API before run the app
setTimeout(() => {
    console.log('dataArr: ', dataArr);
    showProductsView();
    btnHeaderFunction();
    addProducts(dataArr);
    addListenersProdBtn(dataArr);
}, 1500);
