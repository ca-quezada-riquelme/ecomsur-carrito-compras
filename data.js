const apiUrl = 'http://www.omdbapi.com/?apikey=f2d58b74&t=2000';

// we store 12 items from OMDB API.
const urlArr = [
    'http://www.omdbapi.com/?apikey=f2d58b74&t=rashomon',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=shoplifters',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=la+dolce+vita',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=the+conformist',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=things+to+come',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=tampopo',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=only+yesterday',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=ran',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=ikiru',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=amy',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=the+leopard',
    'http://www.omdbapi.com/?apikey=f2d58b74&t=playtime',
]

let dataArr = [];

// this function get data from OMDB API for one item.
// That item is stored in dataArr.
const apiFetch = async url => {
    const res = await fetch(url);
    const data = await res.json();
    dataArr.push(data);
}

// this cycle obtain 12 items from OMDB API
const urlArrLength = urlArr.length;
for (let i = 0; i < urlArrLength; i++) {
    apiFetch(urlArr[i]);
}

// We use an object to store the elements inside the cart.
// They will be stored by id
let dataCart = {};