# Desafío – FrontEnd.
En este desafío se realizó un carrito de compras, utilizando HTML, CSS y Javascript, además del consumo de la API [OMDB](http://www.omdbapi.com/).

## Despliegue
[Clic aquí](https://minyau.neocities.org/ecomsur-carrito-compras/index.html) para ver la aplicación.

## Funcionalidades
- Añadir películas al carrito.
- Eliminar películas añadidas al carrito.
- Volver de la vista del carrito a la vista de las películas disponibles.
- Simular una compra exitosa.

## Mejoras a futuro
- En lugar de ocultar y mostrar las secciones para ver qué películas comprar y ver el carrito, es mejor para el usuario usar un router, porque así puede elegir entre presionar el botón volver o volver dentro del navegador.